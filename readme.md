# Att göra varje gång detta dokument uppdateras
- Uppdatera datum för revideringen
- Uppdatera datum för headern.
- git tag:a commiten för varje uppdatering
  + använd "annotated tags", alltså med `git tag -a`
    * se https://www.atlassian.com/git/tutorials/inspecting-a-repository/git-tag
    * Glöm inte att köra `git push --follow-tags` alternativt `git push --tags`
      * Eller sätt `git config --global push.followTags true` för att slippa
        tänka på det

